`pompy` is a `numpy` based implementation of the puff-based odour plume model described in *Filament-Based Atmospheric Dispersion Model to Achieve Short Time-Scale Structure of Odor Plumes* by Farrell et al. (2002).

It was developed as part of a MSc by Research project in the Insect Robotics group of in the School of Informatics, University of Edinburgh, into whether and how insects are able to use olfactory landmarks to aid in spatial navigation. This project was motivated by the work of Kathrin Steck and colleagues who discovered that *Cataglyphis fortis*, a Saharan desert ant species, are able to use odour sources in their environment as 'landmarks' to help when navigating back to their nest.

### What is this repository for? ###

This `python` package allows simulation of dynamic 2D odour concentration fields which show some of the key characteristics of real chemical plumes in turbulent flows including short term intermittency, diffusive effects and longer term variations in spatial extent and location, while being significantly cheaper to run than a full fluid dynamics simulation.